# HedgeHog CMS

HedgeHog CMS is a <b>C</b>ontent <b>M</b>anagement <b>S</b>ystem built from scratch. The main ideas are taken from a <a href="https://www.a-coding-project.de/ratgeber/php/beispiele/eigenes-cms-erstellen" target="_blank">Tutorial</a> using mysql & php4, written in german. That *is way* to old, but the main concept was ok.


## Requirements

*  PHP 7+
*  PDO / MySQL database
*  Apache module Mod Rewrite


## Planned Features - Basic Core

* Admin Area
    * WYSIWYG Editor
    * Statistics
    * Page Management
    * Photo Upload

* Content
    * Storage in files including data for the header

* Navigation
    * Breadcrump
    * Sidebar
    * Search

* Set Up

